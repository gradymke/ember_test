# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Post.create(title: 'Test Title', author: 'Brandon Grady', intro: 'Nice Intro!', published_at: Date.today, extended: 'Here is a longer text thing.')
Post.create(title: 'Second Test Title', author: 'Brandon Grady', intro: 'Super Nice Intro!', published_at: Date.today, extended: 'Here is a longer text thing.')