# README

## Overview

I made this project in hopes of reproducing the ["Bloggr-Client" from the EmberJS website]
(http://emberjs.com/guides/) with a Rails backend, much like 
[Justin Gordon did from Rails on Maui](http://www.railsonmaui.com/blog/2013/06/11/emberjs-rails4-tutorial/).
Justin's tutorial lacked in that it was using an older version of Rails and Ember, and certain
things have changed since his introduction. He also used CoffeeScript, which I'm philosophically
against as I would rather write in the language that I ultimately have to debug in.

## My Setup

On the Client end, I'm using the following versions:

* Ember: 1.8.0-beta.1
* Ember Data: 1.0.0-beta.10+canary.30d6bf849b
* Handlebars: 1.3.0
* jQuery: 1.11.1

On the Rails end, I'm using the following versions:

* Ruby: 2.1.2
* Rails: 4.1.5
* Ember-Rails: 0.15.0
* Ember-Source: 1.7.0
* Active Model Serializers: 0.9.0

## Key Changes

While following Justin's overview worked OK, the following are some random notes on things I
did differently or things that were omitted from his excellent screencast.

### Setup

Justin opted for the simplistic setup. I wanted to see how an actual Rails/Ember app might 
be structured, so I tried to mimic what was available in the Rails template available at
the Ember site:

    $ rails new my_app -m http://emberjs.com/edge_template.rb

I also split out the various client files into their own files and utilized Sprocket to
fulfill the dependencies.

### Handlebars

Rather than embed the Handlebars templates into one HTML file, I opted to let the Asset Pipeline
compile the files from the javascripts/templates directory by placing them into individual
\*.handlebars files. This is a little tricky as the file names must follow a particular 
convention so the precompiler will name them appropriately.

When proving out the multiple SPA pages (see below), I needed to embed the templates into a directory
deeper than javascripts/templates. This was accomplished by overriding the Resolver class of the
Ember Application object. This can be seen in the code.

### Ember Data

When Justin did his overview, I'm not entirely sure that Ember Data was as far along as it is now.
Since 1.8.0 seems on track to utilize Ember Data, I decided to use it as well. It looks like it 
will be awesome, but it certainly has some minor differences. I've tried to point these 
out in the source comments where applicable.

## Value Additions

Beyond what Justin did, I also proved out how to utilize multiple SPAs rather than only one. 
This was done to prove out a very large application that I might not want to download absolutely
EVERYTHING into, instead I might want to segregate onto multiple pages. One thing I didn't want
to do was to duplicate model definitions, however.

To accomplish this, I have a static controller for each SPA I want to have. I then have an 
"application setup" javascript file at the base assets/javascripts directory. NOTE: Anything 
that is utilized here that is not named "application.js" must be included in the 
config/initializers/assets.rb file (see the source for an example). The JS Application Setup
file sets up the Ember Application for the SPA, overriding the templates Resolver class (see above),
it then initializes the models that the SPA is going to use. Models were defined only a single
time so they can be used in multiple SPAs without having to duplicate code. Lastly it requires
a setup Sprocket JS file that is at the base level of the "page level" directory, which I've put
under javascripts/pages/(page name). This file simply includes that entire directory worth of 
files which are arranged similarly to the Ember template that was used to create the project.

## Contact

To contact me, please e-mail me at [grady.mke@gmail.com](mailto:grady.mke@gmail.com).
