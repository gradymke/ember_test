class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  # TODO - what do we do here? I suppose if we wanted to do some sort of API key thing
  # this would be the place to implement that.
  protect_from_forgery with: :null_session
end
