EmberTest2.PostController = Ember.ObjectController.extend({
  isEditing: false,

  actions: {
    delete: function() {
      if (window.confirm("Are you sure you want to delete this post?")) {
        this.get('model').destroyRecord();
        this.transitionToRoute('posts');
      }
    },

    edit: function() {
      this.set('isEditing', true);
    },

    doneEditing: function() {
      this.set('isEditing', false);
      this.get('model').save();
    }
  }
});
