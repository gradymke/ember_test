EmberTest2.PostsNewController = Ember.ObjectController.extend({
  actions: {
    save: function () {
      var model = this.get('model');
      var self = this;
      // Found this at stack overflow:
      // http://stackoverflow.com/questions/14981500/transition-after-saving-model-of-ember-data
      model.save().then(function() {
        self.transitionToRoute('post', model);
      });
    },

    cancel: function () {
      this.get('model').rollback();
      this.transitionToRoute('posts');
    }
  }
});
