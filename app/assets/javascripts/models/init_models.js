if (typeof AppModelCommon === "undefined") {
  AppModelCommon = {};
}

AppModelCommon.initAllModels = function(emberApp) {
  // All models must be listed here.
  this.initPostModel(emberApp);
}
